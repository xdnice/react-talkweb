# react-talkweb开发文档

------

**主要技术参考**

> * [react~16.2.0(兼容IE9)][1]
> * [react-router^4.3.1(HashRouter兼容IE9)][2]
> * [font-awesome^4.7.0][3]
> * [antd^3.6.2][4]
> * [less^3.0.4][5]
> * [es6(polyfill兼容到IE9)][6]

------

**开发环境搭建**

 1. 编译器
    编译器推荐使用sublime，本文档将围绕sublime来进行说明
    下载地址：[Sublime Text 3][7]，选择对应平台下载安装
 2. 编译器插件
    [LESS-sublime][8](高亮显示less文件)
    [babel-sublime][9](高亮显示react的jsx语法)
    [sublime-jsfmt][10](格式化react代码，此插件需要**手动安装依赖**)
    [Sublime-HTMLPrettify][11](格式化js，css，html，无法格式化jsx语法)
    安装教程及快捷键配置在它们对应的github主页有详细说明
    推荐使用**下载文件**，解压到**Packages**目录安装
 3. 调试工具chrome插件
    建议使用chrome进行调试
    可翻墙去chrome扩展程序商店安装React Developer Tools
    也可下载[React Developer Tools][12]离线安装，安装教程自行百度
 4. nodejs安装
    下载地址：[nodejs][13]，选择对应平台下载安装，默认选项安装无需配置环境变量
 5. npm配置
    nodejs内置了npm，无需手动安装，了解npm请参考：[npm 中文文档][14]
    cnpm为淘宝npm镜像，可大大提高npm安装依赖的速度
    cnpm临时使用：`npm --registry https://registry.npm.taobao.org install express`
    cnpm持久使用：`npm config set registry https://registry.npm.taobao.org`

------

**项目目录结构说明**

> <i class="icon-folder-close"></i> build *`打包编译好的代码目录`*
> <i class="icon-folder-close"></i> config *`webpack打包配置文件夹`*
> <i class="icon-folder-close"></i> node_modules *`依赖目录，npm安装的依赖`*
> <i class="icon-folder-close"></i> public *`webpack编译用到的公共资源`*
> <i class="icon-folder-close"></i> scripts *`npm命令执行文件所在目录`*
> <i class="icon-folder-close"></i> server *`测试用的node接口和静态资源服务，服务根目录为build`*
> <i class="icon-folder-open"></i> src *`源码目录`*
>> <i class="icon-folder-close"></i> components *`公共组件目录`*
>> <i class="icon-folder-open"></i> pages *`公共页面目录（系统页面）`*
>>> <i class="icon-folder-close"></i> 404 *`404页面`*
>>> <i class="icon-folder-close"></i> home *`主页`*
>>> <i class="icon-folder-close"></i> login *`登录页面`*
>>> <i class="icon-folder-close"></i> portal *`项目选址页面`*
>>> <i class="icon-folder-close"></i> welcome *`欢迎页面`*

>> <i class="icon-folder-open"></i> projects *`项目所在目录`*
>>> <i class="icon-folder-open"></i> [ *cardbox* ] *`项目名称`*
>>>> <i class="icon-folder-open"></i> pages *`项目页面`*
>>>>> <i class="icon-folder-open"></i> [ *testpage* ] *`项目功能页面文件夹`*
>>>>>> <i class="icon-folder-close"></i> statics *`本页面单独所用的静态资源，比如图片`*
>>>>>> <i class="icon-file"></i> [ *testpage* ].js *`项目功能页面js`*
>>>>>> <i class="icon-file"></i> [ *testpage* ].less *`项目功能页面样式`*

>>>>> ......

>>>> <i class="icon-folder-close"></i> routers *`每个项目的路由`*

>>> <i class="icon-file"></i> index.js *`项目路由汇总js，每个项目的路由都需要添加进去`*

>> <i class="icon-folder-close"></i> routers *`公共页面路由`*
>> <i class="icon-folder-close"></i> statics *`公共资源，比如图片`*
>> <i class="icon-folder-close"></i> styles *`公共样式`*
>> <i class="icon-folder-close"></i> utils *`公共工具js`*
>> <i class="icon-file"></i> index.js *`入口文件`*
>> <i class="icon-file"></i> registerServiceWorker.js *`资源缓存工具，加快页面速度`*

> <i class="icon-file"></i> package-lock.json *`npm更新日志`*
> <i class="icon-file"></i> package.json *`包管理文件`*
> <i class="icon-file"></i> README.md *`说明文档`*

  [1]: http://react.yubolun.com/
  [2]: http://react-guide.github.io/react-router-cn/docs/API.html
  [3]: http://fontawesome.dashgame.com/
  [4]: https://ant.design/docs/react/introduce-cn
  [5]: http://lesscss.cn/
  [6]: http://es6.ruanyifeng.com/
  [7]: https://www.sublimetext.com/
  [8]: https://github.com/danro/LESS-sublime
  [9]: https://github.com/babel/babel-sublime
  [10]: https://github.com/ionutvmi/sublime-jsfmt
  [11]: https://github.com/victorporof/Sublime-HTMLPrettify
  [12]: http://chromecj.com/downloadstart.html#890
  [13]: https://nodejs.org/zh-cn/download/
  [14]: https://www.npmjs.com.cn/
  [15]: https://www.zybuluo.com/mdeditor?url=https://www.zybuluo.com/static/editor/md-help.markdown
  [16]: https://www.zybuluo.com/mdeditor?url=https://www.zybuluo.com/static/editor/md-help.markdown#cmd-markdown-高阶语法手册
  [17]: http://weibo.com/ghosert
  [18]: http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference